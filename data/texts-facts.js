/*
Copyright 2017 A. "ChickenStorm" Bouttefeux


This file is part of JDRPoly-discord-Bot.

    JDRPoly-discord-Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JDRPoly-discord-Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with JDRPoly-discord-Bot.  If not, see <http://www.gnu.org/licenses/>.


*/


exports.facts = [
		 "e^(i*π)+1=0"
		 ]

exports.jokes = [
		 "Trois logiciens sont dans un bar. Le barman demande s'ils prennent tous une bière.\n Le premier logicien répond \"je ne sais pas\", le deuxième \"je sais pas\" et le troisième répond \"oui\".",
		 "Le problème \"Est-ce que tous les nombres naturels supérieurs à 2, impaires et inférieurs à 10, sont premiers?\" est consécutivement donné à un mathématicien, à un physicien puis à un informaticien.\nLe mathématicien:\"3: ça marche, 5: ça marche , 7: ça marche, 9=3*3 : ça marche pas\".\n Le physicien : \"3: ça marche, 5: ça marche , 7: ça marche, 9: ça marche pas; erreur de mesure donc ça marche \" \n L'informaticien : \"3: ça marche, 5: ça marche , 7: ça marche, 9: ça marche pas,9: ça marche pas,9: ça marche pas,9: ça marche pas,9: ça marche pas,...\"",
		 "Il y a 10 types de personnes : 1) ceux qui comprennent le binaire, 2) ceux qui ne le comprenne pas 3) ceux qui s'attendaient pas à ce que la blague soit en base 3",
		 "f et f' sont sur un bateau. f tombe à l'eau, que fait f'? \nIl dérive...",
		 "Lors d'un entretien d'embauche, on demande à un ingénieur, un informaticien et un étudiant à la Fac de compter jusqu'à 15 : \n\n L’ingénieur : On compte dans IR ou dans IR* ? Et l’ensemble est-il commutatif ?\nL’informaticien : Très bien… 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E… \nL’étudiant à la Fac : Bah… 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, Valet, Dame, Roi…",
		 "Comment se déplace un fakir thermodynamique?\n\n Enthalpie volant",
		 "Un atome d'Hélium rentre dans un bar, le barman répond : Désolé, mais on ne sert pas les nobles. \nL'Helium ne réagit pas",
		 "Combien de techniciens de chez Microsoft ça prend pour changer une ampoule électrique? \n\n - Trois deux pour tenir l'échelle et un pour visser l'ampoule dans le robinet.",
		 "Quelle est la différence entre Jurassik Park et Microsoft ? L'un est un parc de milliardaire ou des gros monstres bouffent tout se qui se trouve sur leur passage. L'autre est un film.",
		 "Les types de femmes en informatique...\n\n La femme DISQUE DUR : \n Elle se rappelle tout, POUR TOUJOURS.\n\n La femme CD-ROM :\n Elle va toujours plus vite avec le temps.\n\n  La femme RAM :\n Elle oublie tout de vous, dès le moment où vous lui tournez le dos.\n\n La femme WINDOWS :\n Tout le monde sait qu'elle ne peut pas faire une chose correctement, mais personne ne peut vivre sans elle.\n\n La femme ECONOMISEUR D'ECRAN :\n Elle est bonne à rien, mais au moins, elle est marrante !\n\n La femme SERVEUR WEB :\n Toujours occupée quand vous avez besoin d'elle.\n\n La femme MULTIMEDIA :\n Elle sait rendre jolies des choses dénuées d'intérêt.\n\n La femme EXCEL :\n On dit d'elle qu'elle peut faire énormément de choses, mais vous l'employez surtout pour gérer votre planning.\n\n La femme COURRIER ELECTRONIQUE:\n Sur dix choses qu'elle dit, neuf sont des pures conneries.\n\n La femme VIRUS :\n Aussi connue sous le nom d'EPOUSE ; quand vous ne l'attendez pas, elle arrive, s'installe et utilise toutes vos ressources. Si vous essayez de la désinstaller, vous perdez forcément quelque chose... Et si vous n'essayez pas de la désinstaller, vous perdez tout...",
		 "Si microsoft inventait quelque chose qui ne plante pas ce serais un clou.",
		 "Un clavier AZERTY en vaut deux.",
		 "Un geek ne crie pas. Il URL !",
		 "Un geek ne s'ennuie pas, il se fichier.",
		 "Windows a détecté que vous n’aviez pas de clavier. Appuyez sur ‘F9’ pour continuer.",
		 "Mieux vaut .tar que .gz"
		 ]//Un atome d'Hélium rentre dans un bar, le barman répond : Désolé, mais on ne sert pas les nobles(édité)
//L'Hélium de réagit pas.

exports.notationApheranne="";

exports.quotes=["\"Tu dois être camouflé. Donc ton épée, ca passe pas\" (modèle épée de Zack, FF)\"Mais ... c'est pour mon asthme ...\"",
]

//"\n",
// "\n (Livre I, \"Heat\")"
// quotes from Kaamelott
exports.kaamQuotes =[
	"Perceval (à Arthur) : \"Faut faire comme avec les scorpions qui se suicident quand ils sont entourés par le feu, faut faire un feu en forme de cercle, autour d'eux, comme ça ils se suicident, pendant que nous on fait le tour et on lance de la caillasse de l'autre côté pour brouiller... Non ?\" \n (Livre I, \"Heat\") ",
	"Perceval : \"Si on faisait le coup du bouclier humain ? Par exemple, Sire, Léodagan et moi on fait semblant de vous prendre en otage : on vous met une dague sous le cou et on traverse le camp adverse en gueulant : \" Bougez pas, bougez pas ou on bute le Roi!\"...\"\n (Livre I, \"Heat\")",
	"Perceval : \"Putain, en plein dans sa mouille !\"\n (Livre I, \"Heat\")",
	
	"Perceval : \"Si c'est le même volume sonore, on dit équidistant. S'ils sont équidistants en même temps que nous, on peut repérer le dragon par rapport à une certaine distance. Si le dragon s'éloigne, on sera équidistant, mais ça sera vachement moins précis et pas réciproque.\" \n (Livre I, \"La fureur du dragon\")",
	
	"Perceval (à Arthur et Lancelot) : \"Une fois, à une exécution, je m'approche d'une fille. Pour rigoler, je lui fais : Vous êtes de la famille du pendu ? C'était sa soeur. Bonjour l'approche !\"\nLivre I, \"La coccinelle de Madenn\")",
	"Perceval (à Arthur et Lancelot) : \"Ça sert à rien, un siège, si elle est enceinte, il faut des linges blancs et une bassine d'eau chaude.\"\nLivre I, \"La coccinelle de Madenn\")",
	
	"Perceval : \"Faut arrêter ces conneries de nord et de sud ! Une fois pour toutes, le nord, suivant comment on est tourné, ça change tout !\"\n(Livre I, \"Ambidextrie\")",
	"Perceval : \"Toi un jour je te crame ta famille, toi.\"\n(Livre I, \"Ambidextrie\")",
	
	"Perceval : Donc, pour résumer, je suis souvent victime des colibris, sous-entendu des types qu’oublient toujours tout. Euh, non... Bref, tout ça pour dire, que je voudrais bien qu’on me considère en tant que tel.\n(Livre I, \"Tel un chevalier\")",
	
	"Perceval : \"On a même un tabouret ! Quand on s'assoit dessus, on se retrouve sur un autre tabouret dans une taverne dans le Languedoc ! Ouais, le siège de transport qu’ils appellent. En plus, comme par hasard c’est moi qui ai essayé le premier. Deux semaines et demie plus le bateau qu'ça m'a pris pour revenir. J’avais pas compris qu’en me rasseyant dessus, ça me ramenait de l’autre côté. Et à l’arrivée j’me suis fait mettre une chasse, parce que j’avais ramené l’autre tabouret, et que soit-disant il aurait fallu qu’il reste là-bas. Pourtant ils marchent les deux tabourets ! Eh ben ils sont l’un à côté de l’autre. Alors ça fait pas pareil.\"\n(Livre I, \"Le chaudron rutilant\")",
	"Perceval : \"C’est pour ça : j’lis jamais rien. C’est un vrai piège à cons c’t’histoire-là. En plus j’sais pas lire.\"\n(Livre I, \"Le chaudron rutilant\")",
	
	"Perceval : \"Si Joseph d'Arimathie a pas été trop con, vous pouvez être sûr que le Graal, c'est un bocal à anchois.\"\n(Livre I, \"En forme de Graal\")",
	
	"Perceval : \"C'est pas faux!\"\n(Livre I, La Botte secrète)",
	
	"Perceval : \"Excusez, c’est juste pour vous dire que je vais pas pouvoir rester aujourd’hui ! Faut que je retourne à la ferme de mes vieux ! Y a ma grand-mère qui a glissé sur une bouse ! C’est le vrai merdier !\"\n(Livre I, \"Le Code de Chevalerie\")",
	
	"Perceval : (Assommant Karadoc) \"Ah ! oui... j' l'ai fait trop fulgurant, là. Ça va ?\"\n(Livre I, \"Unagi\")",
	
	"Perceval : (À Arthur) \"Sire, avec tout le respect, est-ce que la reine a les fesses blanches ?\"\n(Livre I, \"Les Fesses de Guenièvre\")",
	
	"Perceval : \"Ils auraient pu se creuser la tronche pour trouver un autre nom, chez pas moi... chevalierisation...\" (silence) \"Quoi, c'est déjà pris ?\"\n(Livre I, \"L'Adoubement\")",

	//----------------
	
	"Perceval (à Arthur) : \"Moi, j'serais vous, je vous écouterais... Non, moi, j'serais nous, je vous... Si moi j'étais vous, je vous écouterais ! Non, elle me fait chier, cette phrase !\"\n(Livre II, \"Les exploités\")",
	"Perceval : \"Sire, Sire ! On en a gros !\"\n(Livre II, \"Les exploités\")",
	
	"Perceval : \"C’est marrant les petits bouts de fromage par terre. C’est ça que vous appelez une fondue ?\"\n(Livre II, \"Séli et les Rongeurs\")",
	
	"Perceval : \"J'voudrais pas faire ma raclette, mais la soirée s'annonce pas super.\"\n(Livre II, \"Un Roi à la Taverne II\")",
	
	"Perceval (en parlant de la date de son anniversaire) : \"Je le dis pas. À l'époque quand je le disais, tout le monde oubliait de me le souhaiter. Ça me faisait pleurer. Ça m'a gonflé, j'ai arrêté.\"\n(Livre II, \"Le Tourment II\")",

	"Perceval : \"Dans la vie, j'avais deux ennemis : le vocabulaire et les épinards. Maintenant j'ai la botte secrète, et je bouffe plus d'épinards. Merci de rien, au revoir messieurs dames.\"\n(Livre II, \"La botte secrète II\")",

	"Perceval (En parlant des clients de la taverne)  : \"Quand même, ils sont onze. J'ai calculé sur les treize dernières années, dans les deux heures qui précèdent le coucher du soleil, vous en êtes à une moyenne de 8,422.\"\n(Livre II, \"Sept cent quarante-quatre\")",
	"Perceval : \"Après demain, à partir d'aujourd'hui ?\"\n(Livre II, \"Sept cent quarante-quatre\")",
	"Perceval : \"De toutes façons, les réunions de la Table Ronde c’est deux fois par mois. Donc, si le mec il dit après-demain à partir de dans deux jours, suivant s’il le dit à la fin du mois, ça reporte.\"\n(Livre II, \"Sept cent quarante-quatre\")",
    
	"Perceval : \"C’qui compte, c’est les valeurs !\"\n(Livre II, \"Perceval et le Contre-sirop\")",
	"Perceval : \"Là, vous faites sirop de vingt-et-un et vous dites : beau sirop, mi-sirop, siroté, gagne-sirop, sirop-grelot, passe-montagne, sirop au bon goût.\"\n(Livre II, \"Perceval et le Contre-sirop\")",
	
    "Perceval : \"LEODAGAN CONTRE-ATTAQUE !!!\" (il passe la balle à Léodagan)\n(Livre II, \"The Game\")",
	
	"Perceval : \"Et toc ! Remonte ton slibard, Lothard !\"\n(Livre II, \"L’Absent\")",

	"Perceval : \"13, 14, 15... Enfin tous les chiffres impairs jusqu'à 22.\"\n(Livre II, Unagi II)",

	"Perceval : \"Salut, Sire. Je trouve qu’il fait beau, mais encore frais, mais beau !\n(Livre II, \"Les Volontaires II\")",

	"Perceval (Montrant ses cousins à Arthur): \"Alors là, c'est Ashton, lui c'est Rutz, lui c'est Pierce, l'autre derrière c'est Pierce aussi, j'ai jamais compris pourquoi, et lui euh, j'crois qu'il a pas d'prénom, tout le monde l'appelle Connard...\"\n(Livre II, \"La Garde Royale\")",

	"Perceval : \"PAYS DE GALLES INDÉPENDANT ! J'ai un pivert dans la tête... C'est normal ?\"\n(Livre II, \"L’Ivresse\")",
	
	"Perceval : \"Karadoc, c'est le gars brillant. Le frère, à côté, c'est sûr... C'est vraiment un gros con.\"\n(Livre II, \"O'Brother\")",
	
	"Perceval : \"Ben j'en ai marre. Ça revient à chaque fois sur le tapis ça.\"\n"
	+"Arthur: \"Quoi ça ?\"\n"
	+"Perceval : \"Fédéré! D'habitude j'dis rien mais là zut! J'sais pas c'que ça veut dire. Moi j'veux bien faire des efforts pour comprendre les réunions mais faut que chacun y mette du sien aussi. Là on est partis pour une heure avec des fédérés par-ci des fédérés par-là, j'vais encore rien biter et ça me gonfle.\"\n(Livre II, \"L'Alliance\")",
	
	
	"Perceval (À Arthur) : \"C'est l'anniversaire dans tous les recoins, c'est presque tous les ans qu'on a l'anniversaire. Grâce à cet anni... c'est la joie c'est pratique, c'est au moins un principe à retenir pour faire la frite... c'est huuuum lalalalala. Cette année c'est bien, l'anniversaire tombe à pic !\"\n(Livre II, \"Les Félicitations\")",
	
	
	//------------------------------
	
	"Perceval : \"Sans blague on pourrait pas fêter la mort des mecs que je connais pour une fois ? Comment ça ? C'est toujours la mort de vos potes à vous que l'on fête, moi dans quatre jours c'est l'anniversaire de la mort d'un oncle à moi, sans faire exprès il s'est tiré dessus avec un arc.\"\n(Livre III, \"Le Jour D’Alexandre\")",

	"Perceval : \"Le Graal, c’est une vraie saloperie, méfiez-vous. Un jour c’est un vase, une semaine après une pierre incandescente.\"\n(Livre III, \"Les Suppléants\")",
	"Perceval : \"Incandescente, c’est : qui peut accaparer des objets sans resurgir sur autrui.\"\n(Livre III, \"Les Suppléants\")",
    

	"Perceval : \"Non, vous, vous vous maravez. Quand on a pas de technique, il faut y aller à la zob.\"\n(Livre III, \"Morituri\")",

	"Perceval : \"SI VOUS VOULEZ QU'ON SORTE LES PIEDS DEVANT, FAUDRA NOUS PASSER SUR L'COOOORPS !\"\n(Livre III, \"La menace fantôme\")",

	"Perceval : \"Le code c'est \"le code\" ? Ça va, ils se sont pas trop cassés le bonnet, pour l'trouver celui-là \"\n(Livre III, \"Poltergeist\")",
	"Perceval : \"Y'a du grabuge alors on appelle les 2 couillons... On met les glandus à profit !\"\n(Livre III, \"Poltergeist\")",
	"Perceval : \"Les framboises sont perchées sur le tabouret de mon grand-père.\"\n(Livre III, \"Poltergeist\")",
      

	"Perceval : \"Ça prouve que j'ai de l'ubiquité... De l'humilité ? C'est pas quand il y a des infiltrations ?\"\,(Livre III, L'Etudiant)",
	"Perceval : \"À ROULEEEEETTES !! HOULA... J'l'ai un peu trop gueulé ça, non ? À roulettes.\"\,(Livre III, L'Etudiant)",
	"Perceval : \"Ah, mais c'est de là que ça vient ! Quand on dit \"ça va comme sur des roulettes\". En fait ça veut dire qu'le mec il peut balancer un morceau de rocher comme une catapulte, il continue quand même d'avancer d'façon mobile.\"\,(Livre III, L'Etudiant)",
	"Perceval : \"Une maquette ?! Vous avez pas dit qu'c'était une catapulte ?\"\,(Livre III, L'Etudiant)",
       

	"Perceval : \"Les 3 actes, c'est les bonnes femmes qui sont mi-taupes mi-déesses, et qui ont forcé les mecs de Bethléem à construire les pyramides.\"\n(Livre III, La poétique, 1ère partie)",
	"Perceval : \"Dans le Languedoc, ils m'appellent Provençal. Mais c'est moi qui m'suis gouré en disant mon nom. Sinon, en Bretagne, c'est le Gros Faisan au sud, et au nord, c'est juste Ducon ...\"\n(Livre III, \"Le Sanglier De Cornouailles\")",

	"Perceval : \"Est-ce que vous avez assez de dés pour remplir un seau comme ça ? (Le tavernier n'en a que trois) C'est plus facile d'en tirer cent ou deux-cents d'un coup et d'additionner, parce que là avec trois, on va être obligés de tirer quarante fois de suite à chaque tour.\n(Livre III, \"Chante Sloubi\")",
	"Perceval : \"Sloubi 1, sloubi 2, sloubi 3, sloubi 4, sloubi 5 [...] sloubi 324, sloubi 325!...\"\n(Livre III, \"Chante Sloubi\")",
	"Perceval : \"C'est moi qui remporte le tour. Quand on remporte le tour à Sloubi, on a quatorze solutions possibles : soit on annule le tour ; soit on passe ; soit on change de sens ; soit on recalcule les points ; soit on compte ; soit on divise par six ; soit on jette les bouts de bois de quinze pouces, ça c'est quand on joue avec les bouts de bois ; soit on se couche ; soit on joue sans atouts. Et après y'a les appels : plus un ; plus deux ; attrape oiseaux ; régoudon ; ou chante Sloubi. [...] Comme vous êtes second, vous avez plus que dix-neuf solutions possibles : soit vous passez ; soit vous sciez en deux les cinquante poutrelles de trente pieds, mais ça c'est quand on joue avec les bouts de bois. Sinon c'est les relances : doublette ; jeu carré ; jeu de piste ; jeu gagnant ; jeu moulin ; jeu-jeu ; joue-jeu ; joue-joue ; joue-jié ; joue-ganou ; gagnar ; catakt ; tacat ; cacatac ; cagat-cata et ratacat-mic. Ou : chante Sloubi. Nous, on va faire que chante Sloubi.\"\n(Livre III, \"Chante Sloubi\")",

	"Perceval : \"Je vous ai vu une fois dans une carriole, tirée par un cheval. Enfin, la carriole tirée par un cheval.\"\n(Livre III, \"L'Empressée\")",

	//-----------------------------------------------------
	
	"Perceval : \"Mais moi, j'm'en fous des honneurs, rien à péter, le Graal aussi, rien à péter. Moi, c'est Arthur qui compte. Moi je suis pas un as de la stratégie ou du tir à l'arc, mais je peux me vanter de savoir ce que c'est d'aimer quelqu'un.\""
	+"\n(Livre IV, \"L'habitué\")",

	"Perceval : \"Au printemps, j’aime bien pisser du haut des remparts au lever du soleil… Y’a une belle vue !\""
	+"\n(Livre IV, \"Tous les matins du monde, 2e partie\")",
	
	"Perceval : \"Sur une échelle de 2 à 76, et là je préfère prendre large, de 2 à 71 on ne nous écoute pas, de 72 à 75, on nous écoute toujours pas, et seulement à 76 on nous laisse parler sans nous engueuler.\""
	+"\n(Livre IV, \"L’Échelle de Perceval\")",
	
	"Perceval : \"Mais cherchez pas à faire des phrases pourries... On en a gros, c'est tout !\""
	+"\n(Livre IV, \"Les Exploités II\")",
	
	
	"Perceval : \"En plus je connais une technique pour tuer trois hommes en un coup rien qu'avec des feuilles mortes ! Alors là, vous êtes deux, vous avez bien de la chance.\""
	+"\n(Livre IV, \"L'Inspiration\")",
	"Perceval : \"Moi, la canne, ça m’aide. Je visualise le caillou dans l’eau, j’ai l’impression de faire partie d’un tout, moi, le caillou, le fil, le lac, le ciel, c’est entier, vous comprenez ? C’est bien fini. C’est pour ça, moi je me dis, c’est dans ces moments-là qu’on peut bien comprendre des trucs. Vous me prenez pour un con, non ?\""
	+"\n(Livre IV, \"L'Inspiration\")",
	"Perceval : \"Ah ça y’est, j’viens de comprendre à quoi ça sert la canne. En fait ça sert à rien… Du coup ça nous renvoie à notre propre utilité : l’Homme face à l’Absurde !\""
	+"\n(Livre IV, \"L'Inspiration\")",
	
	"Perceval : \"C'est pas moi qui explique mal c'est les autres qui sont cons !\""
	+"\n(Livre IV, Perceval Fait Ritournelle)(modifié)",
	
	"Perceval : \"Vous vous prenez pour un enseignant ?... Non j' s'entais que c'était le moment d'faire une vanne mais y'a rien qui est sorti.\""
	+"\n(Livre IV, \"Les chaperons\")",
	"Perceval : \"Bon ça suffit maintenant ! Vous voulez qu'j'me foute en rogne comme un enseignant ? ... Qu'est ce que j'ai avec ça moi ?\""
	+"\n(Livre IV, \"Les chaperons\")",
	
	"Perceval : \"Ils ont pas de bol, quand même ! Mettre au point un truc pareil et tomber sur des cerveaux comme nous !\""
	+"\n(Livre IV, \"Les Pisteurs\")",
	
	//-----------------------------
	
	"Perceval : \"On va pas installer notre carré germinal à la taverne!\""
	+"\n(Livre V, \"L'Épée des Rois\")",
	
	"Perceval (à Mevanwi) : \"Elle a compris la vilaine frisée ? On a dans l'projet de fonder un clan autonome pour partir à l'aventure et ramener du pognon pour entretenir vos grosses miches ! Alors le cageot il dit merci et il ferme sa boîte à caca !\""
	+"\n(Livre V, \"L'épée des Rois\")",
	 
	"Perceval : Vous, vous avez une idée derrière la main, j'en mettrais ma tête au feu !"
	+"\n(Livre V, \"Perceval de Sinope\")",
	 
	"Perceval : \"Je vais vous poser une série de questions. Vous répondez par oui, non, ou Zbradaraldjan. Ok c'est parti : où se trouve l'oiseau ?... Allez c'est facile ça. Trouve pas ? Bon tant pis. C'était \"sur la branche\". Eh oui, y a des pièges.\""
	+"\nPerceval : \" Si la mémoire est à la tête ce que le passé, peut-on y accéder à six ? Oui, non, zbradaraldjan ?\""
	+"\nPerceval : \"Ma tante me demande de trouver un endroit pour y entreposer 667 noix. A la cave il y a de la place pour 595, à la remise il y a la place pour 337. Qu'est-ce que je fais ? Je les ?... Allez on cherche bon dieu ! Je les... Zbradaraldjan le grenier!... Allez il dégage le bourrin !\""
	+"\nPerceval : \"Et si je sens que y a des anguilles à la broche, dehors ! Comme César quand il a chassé les marchands du temple, et qu'ils ont foutu le camp sur le bateau avec les bestioles et l'pépé.\""
	+"\n(Livre V, \"Les Recruteurs\")",
	
	"Perceval : \"Je crois que c'est rentré par là, et c'est ressorti par là ; et c'est re-rentré par là, et c'est RE-RE-SORTI PAR LA. ET NOUS ON S'SAIGNE AUX QUATRE FROMAGES !!!\""
	+"\n(Livre V, \"Domi Nostrae\")",
	
	"Perceval : \"Votre femme, si j'avais pas la flemme de descendre de là, elle aurait pris mon pied dans son cul depuis un moment. Parce y'a un truc qu'on oublie quand on parle de retirer Excalibur : c'est le respect au Roi Arthur! Et le respect au Roi Arthur je remarque que Madame en avait un peu plus quand elle était dans son PLUMARD !! \""
	+"\n(Livre V, \"Les Dauphins\")",
	
	"Perceval : \"Si on avait bu un coup dans des trucs qui s'cassent, j'en aurais pété un par terre avant d'monter dans ma chambre, pour bien montrer comment j'suis colère.\""
	+"\n(Livre V, \"La Promesse\")",
	
	"Perceval : \"Progressif... N'oubliez pas, dans la casse, le plus important, c'est les suites d'épaisseurs ... Bûche de 10, Bûche de 16; Bûche de 32, Bûbûche, Bibuchette, et re-Bûche de 6 !!!\""
	+"\n(Livre V, \"Unagi V\")",
	
	"Perceval : \"On a une autorité naturelle, il faut en profiter... J'suis sûr que même à poil on ferait toujours chef !\""
	+"\n(Livre V, \"Le Royaume Sans Tête\")",
	
	
	"Perceval : \"Ouais ouais ouais ! Il faudrait peut-être commencer par se comporter en adulte !\""
	+"\n(Livre V, \"Les Exilés\")",
	
	//------------------------------------
	
	"Perceval : \"Allez, y'a plein de bruit, là ! Si ça se trouve c'est bourré d'oiseaux venimeux. Y'en a des rouges, des jaunes, des re-rouges et des pourpres ! Y bouffent que des noisettes et des escalopes de veau. Et quand ils vous donnent un coup de bec vous voyez une grande lumière et ça vous donne la diarrhée !\""
	+"\n(Livre VI, \"Dux Bellorum\")",
	"Perceval : \"Nan mais je l'ai déjà impressionné, moi ! Je lui ai expliqué une nouvelle technique de combat : on se bat à moitié à mains nues, et à moitié avec du calcium. J'peux vous dire il faisait moins le malin !\""
	+"\n(Livre VI, \"Dux Bellorum\")",
	
	"Perceval (à Arthur): \"Ouais en même temps ça vous a prouvé qu'on avait pas froid au ventre !\""
	+"\nArthur : \"Aux yeux!\""
	+"\nPerceval : \"Comment ?\""
	+"\nArthur : \"Aux yeux, pas froid au ventre.\""
	+"\n[...]"
	+"\nPerceval : \"Ben si, à la limite avec du vent… mais bon si on a froid aux yeux on les ferme !\""
	+"\n(Livre VI, \"Arturus Rex\")",
	
	"Perceval (à Arthur qui vient de lui raconter longuement un rêve): \"C'est vraiment chouette comme rêve... Moi l'autre nuit j'ai rêvé que Karadoc avait des pinces...\""
	+"\nArthur : (confus) \"C'est-à-dire?\""
	+"\nPerceval : \"Comme un crabe...\""
	+"\nArthur : \"Ah? Et qu'est-ce qu'il faisait avec ses pinces?\""
	+"\nPerceval : \"Il me pinçait le ménisque.\""
	+"\nArthur (interloqué) : \"Le ménisque ?\""
	+"\nPerceval : \"Comparés aux vôtres, ils sont pourris mes rêves, ou pas?\""
	+"\n(Livre VI, \"9\")",
	
	
	//----------------------------------------
	
	"Karadoc : \"On construit un barrage, après on lance de la caillasse de l'autre côté de la rivière pour faire croire aux autres qu'on a traversé dans l'autre sens. Une fois qu'ils sont au milieu, on casse le barrage et on les noie. Ouais... le seul problème c'est que quand on a passé quatre semaines à construire un barrage, ça fait un peu mal au cul d'le détruire...\""
   +"\n(Livre I, \"Heat\")",

	"Karadoc : \"Mais y a rien à développer ! C'est de la merde, c'est de la merde, c'est tout ! Moi, on me sert ça dans une auberge, le tavernier, il s'prend une quiche dans sa tête !\""
	+"\nKaradoc : \"Ce pain-là, il est cuit trop vite dans un four trop chaud : la montée a pas le temps de se faire et il y a trop d'air dans la mie. Donc c'est de la merde.\""
	+"\n(Livre I, \"Le pain\")",
	 
	"Karadoc : \"L'agneau était daubé du cul !\""
	+"\n(Livre I, \"La quête des deux renards\")",
	//------------------------
	
	"Karadoc : \"Eh oui mémé, t'es bien mouchée!\""
	+"\n(Livre II, \"L'absent\")",
	
	"Karadoc : \"Par exemple, vous prenez aujourd’hui. Vous comptez sept jours. Ça vous emmène dans une semaine. Et bien on sera exactement le même jour qu’aujourd’hui. À une vache près, hein... C’est pas une science exacte.\""
	+"\n(Livre II, \"Sept cent quarante-quatre\")",
	"Karadoc : \"Des petits croutons tout vieux genre pour les lapins ? Ouais j'savais pas c'que c'était, dans le doute, j'les ai bouffés.\""
	+"\n(Livre II, \"Sept cent quarante-quatre\")",
	
	"Karadoc (en parlant de la politique de l'autruche) : \"Une politique qui court vite, une politique qui fait des gros œufs, c'est tout.\""
	+"\n(Livre II, \"La cassette\")",
	
	"Karadoc : \"Les chicots c'est sacré ! Parce que si on en prend pas soin, dans dix ans, c'est tout à la soupe. Et celui qui me fera bouffer de la soupe, il est pas né !\""
	+"\n(Livre II, \"Immaculé Karadoc\")",
	
	"Karadoc : \"Qu’est-ce que c’est que ce style de bouffer des petits machins tout secs et trois gallons de flotte par jour ? Si la jeunesse se met à croire à ces conneries, on se dirige tout droit vers une génération de dépressifs ! Le gras, c’est la vie.\""
	+"\n(Livre II, \"Corpore sano\")",
	
	"Karadoc : \"Sire ! Enfin vous arrivez pour me sauver. De l’hypolipémie ! J’ai plus de gras dans le sang. Je vais me mettre à peler et à perdre mes cheveux...\""
	+"\nKaradoc : \"Ça y est je vois trouble. C’est le manque de gras, je me dessèche.\""
	+"\n(Livre II, \"La restriction\")",
	
	"Karadoc : \"Vous nous utilisez bon gré malgré pour arriver sur la fin.\""
	+"\nKaradoc : \"Le Graal par ci, le Graal par là. Le Graal par ci, le Graal par là. Le Graal par ci, le Graal par là. Le Graal par ci, le Graal par là...\""
	+"\n(Livre II, \"Les Exploités\")",
	
	"Karadoc : \"Mon frère, il peut pas aller à l'école. Quand on lui explique un machin technique, il s'évanouit.\""
	+"\n(Livre II, \"O'Brother\")",
	//----------------------------------------
	
	"Karadoc : \"C'est normal, c'est en se cassant la gueule qu'on apprend à marcher. Combien de fois j'ai failli m'étouffer avec un os de lapin. Il faut jamais se laisser abattre par un échec, c'est ça le secret.\""
	+"\n(Livre III, \"Le tourment III\")",
	"Karadoc : \"La neige qui poudroie dans la solitude de notre enfance. Qu'est-ce que vous dites de ça ?\""
	+"\n(Livre III, \"Le tourment III\")",
	
	"Karadoc : \"Oh le con ! Mais il est pas fini d'affiner !\""
	+"\n(Livre III, \"La grande bataille\")",
	
	"Karadoc : \"On tombe sur le fric, comme ça, du premier coup ! On s'est même pas fait un cor au pied !\""
	+"\n(Livre III, \"La menace fantôme\")",
	
	"Karadoc : \"Cette histoire de Graal, ça a assez traîné ! Si c’est pas moi qui prend les choses en main, on y est encore dans cent piges ! Préparez la fiesta, j’suis un héros !\""
	+"\n(Livre III, \"La corne d'abondance\")",
	
	"Karadoc : \"Parce que d'un point de vue santé publique, il vaut mieux bouffer ça une fois par mois que de la merde tous les jours, je vais vous dire, à ce niveau là, c'est plus de la gastronomie, c'est de l'érotisme.\""
	+"\n(Livre III, \"Cuisine et dépendances\")",
	
	"Karadoc : \"La joie de vivre et le jambon, y'a pas trente-six recettes du bonheur !\""
	+"\n(Livre III, \"Le législateur\")",
	
	"Karadoc : \"Parce que mon couteau pour le pâté... euuh... y'a rien à faire, jm'en tape.\""
	+"\n(Livre III, \"Le Porte Bonheur\")",
	//--------------
	 
	"Karadoc : \"Tout à l’heure, on a vu que le chapelet de saucisses n’était pas un objet redondant. Et pourtant, on a pu lui trouver une utilisation périmétrique en s’en servant comme un fouet.\""
	+"\nKaradoc : \"Lorsqu'on le tient par la partie sporadique, ou boulière, le fenouil est un objet redondant.\""
	+"\n(Livre IV, \"Unagi IV\")",
	
	"Karadoc : \"Là, y’a les méduses, les insectes. Là, y’a les glandus, les grouillots. Là, y’a les mecs normaux. Là, y’a les chevaliers. Là, y’a les rois et les princes. Et après, bien au-dessus, y’a le roi Arthur. Vous, vous aurez eu deux bonhommes dans votre vie, eh ben vous pourrez dire que vous avez tapé dans l’exception.\""
	+"\n(Livre IV, \"Duel 2e partie)",
	
	"Karadoc : \"Ma femme... mon ex-femme... C’était peut-être un peu moins prestige parce qu’elle est pas reine... mais au moins, elle habitait pas à six heures de marche avec un autre mec.\""
	+"\nKaradoc : \"C'est pas que c'est difficile de la récupérer... C'est que c'est sa mère difficile de la récupérer, la race de sa grand-mère !\""
	+"\n(Livre IV, Le vice de forme)",
	
	"Karadoc : \"Allez, faites-vous belle, que j'me pointe avec la came présentable.\""
	+"\n(Livre IV, L’Échange 1re partie)",
	
	"Karadoc : \"Du passé faisons table en marbre.\""
	+"\n(Livre IV, L’Échelle de Perceval)",
	"Karadoc : \"Vous dites pas : « Qu’est ce qu'il fait chaud… », vous dites : « La chaleur est un plat qui se mange froid. »\""
	+"\n(Livre IV, L’Échelle de Perceval)",
	//----------------------
	
	"Karadoc : \"Les chiffres, c'est pas une science exacte figurez-vous!\""
	+"\nKaradoc : \"C’est compliqué mais c’est compliqué !\""
	+"\n(Livre V, Le dernier recours)",
	
	"Karadoc : \"Quand je pense à la chance que vous avez de faire partie d'un clan dirigé par des cerveaux du combat psychologique, qui se saignent...aux quatre parfums du matin au soir !!\""
	+"\n(Livre V, Domi Nostrae)",
	
	"(Perceval: Si vous foutez tout en l'air, on vous envoie notre assassin, ça va pas traîner.)"
	+"\nKaradoc : \"Ouais. Ou alors le mec qu'on dirait qu'il marche normalement mais en fait il marche alternativement à cloche-pied sur chaque pied.\""
	+"\n(Livre V, \"La Roche Et Le Fer\")",
	
	//-----------------------------
	
	
	"Maître d'armes : \"HAHA, Sire ! Je vous attends ! À moins que vous préfériez que l’on dise partout que le roi est une petite pédale qui pisse dans son froc à l’idée de se battre !\""
	+"\n(Livre I, \"Le maître d’armes\")",
	"Maître d'armes : \"Sire ! Mon père est peut-être unijambiste, mais moi, ma femme n'a pas de moustache !\""
	+"\n(Livre I, \"Le maître d’armes\")",
	"Maître d'armes : \"En garde, espèce de vieille pute dégarnie !\""
	+"\n(Livre I, \"Le maître d’armes\")",
	 
	//-------------------------
	
	"Maître d'armes : \"JE NE MANGE PAS DE GRAINES !\""
	+"\nMaître d'armes : \"J'estime que si on avale l'équivalent de son poids en viande deux fois par jour, il ne faut pas s'étonner de ne pas pouvoir mettre un pied devant l'autre sur un champ de bataille.\""
	+"\n(Livre II, \"Corpore sano\")",
	 
	"Maître d'armes : \"Moi, une fois, j'étais soûl comme cochon, je me suis fait tatouer \"J'aime le raisin de table\" sur la miche droite, et ça y est toujours !\""
	+"\n(Livre II, \"L'ivresse\")",
	 
	"Maître d'armes : \"Non, je veux dire « malade mental », c'est votre maximum, comme insulte ? Non parce qu'il va falloir passer le cran au-dessus, mon vieux, parce que sinon, on y est encore demain !\""
	+"\nMaître d'armes : \"Mais allez-y bon sang, magnez-vous le fion, espèce de grosse dinde !\""
	+"\nMaître d'armes : \"Seigneur Bohort! Je commence à en avoir ras le bol de votre comportement de péteux alors vous allez me faire le plaisir de me faire une bonne insulte et de vous foutre en rogne une bonne fois pour toutes!\""
	+"\n(Livre II, \"Les classes de Bohort\")",
	
	"Maître d'armes : \"En garde, ma biquette ! Je vais vous découper le gras du cul, ça vous fera ça de moins à trimbaler !\""
	+"\n(Livre II, \"Excalibur et le Destin\")",
	 
	//--------------------------------
	
	"Maître d'armes : \"Quand on est idiot, on plante des carottes on ne s'occupe pas de sécurité !\""
	+"\n(Livre III, \"L'assemblée des Rois 2e partie\")",
	 
	//----------------------------------
	
	"Maître d'armes : \"Le bon roi Arthur est une p'tite tapette. Le bon roi Arthur est une p'tite tapette. Est une p'tite à la volette, est une p'tite à la volette, est une p'tite tapette.\""
	+"\nMaître d'armes : \"Du nerf, mon lapinou !… Vous allez vous faire tailler le zizi en pointe !\""
	+"\nMaître d'armes : \"ALLEZ, EN GARDE GROSSE CONNE ! Non, ça va pas, ça va pas !\""
	+"\nMaître d'armes : \"Vous savez quoi, Sire ? On va commencer par se faire une saucisse grillée de trois pieds de long, avec un tonnelet de pinard chacun, et derrière, peut être bien qu'on se paiera des filles. Ah oui ! A un moment, vive la vie !\""
	+"\n(Livre IV, \"Corpore sano II\")",
	 
	//--------------------------
	
	"Maître d'armes : \"Euh, juste une chose... Manquez encore une seule fois de respect au futur roi de Bretagne, et je vous coupe les boules ! Ca vous fera une jolie petite sacoche pour ranger vos dés à coudre.\""
	+"\nMaître d'armes : \"Je suis, je suis, je suis une petite tapette, qui parle à tort et à travers, sans que personne ne lui demande son avis, alors elle ferme son bec la poupoule... Et elle laisse parler les grands garçons.\""
	+"\n(Livre VI, \"Nuptiæ\")",
	 
	"Maître d'armes : \"Regardez moi la jolie petite paire de fillettes, si c'est pas fragile!\""
	+"\n(Livre VI, \"Arturus Rex\")",
	
]
