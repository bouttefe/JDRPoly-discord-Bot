
var messageEmbed = []; // array of all messageEmbed

exports.messageEmbedArray = messageEmbed;

//TODO trouver comment faire mieux
//
exports.init = function(bot){
	
	messageEmbed[bot.user.id] = {embed :{
		//url: "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot/wikis/liste-des-commandes",
		color: 3447003,
		author: {
			name: bot.user.username,
			icon_url: bot.user.avatarURL,
			url : "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot"
		},
		title: "Fiche de perso",
		timestamp: new Date(),
		footer: {
			icon_url: "https://cdn.discordapp.com/avatars/93784478299725824/49fa5e6b2ebbfdac9736790a49000ef7.png?size=2048",
			text: "© A. \"ChickenStorm\" Bouttefeux",
		},
		fields : [
				  {
					name : "Nom",
					value : "Merlin, Golem enchanteur"
				  },
				  {
					name : "Race",
					value : "Golem de pierre"
				  },
				  {
					name : "Rôle",
					value : "Enchanteur"
				  },
				  {
					name : "Physique",
					value : "Un gros golem en pierre couvert de rune bleu. Il a toujours sur lui des crystal pour faire de l'enchantement."
				  },
				  {
					name : "Caractère",
					value : "En tant que golem, Merlin est calme et se contente de faire ce pourquoi il a été créé."
				  },
				  {
					name : "Histoire",
					value : "Merlin fut crée par le nouveau golemancien du vilage ChickenStorm."
				  },
				  {
					name : "Magie",
					value : "Merlin maitrise l'art noble de l'enchatement."
				  },
				  {
					name : "Capacités",
					value : "Merlin est capable d'enchanter presque tout les objets. Vous avez besion d'une chope +1 au jet, allez voir Merlin. Ses arcanes d'enchatement lui permet surtout de réparer différents objet"
				  },
				  
				  ],
		thumbnail: {
			url:  bot.user.avatarURL,
		},
		image:{
			url:  bot.user.avatarURL,
		},
		
	}};
	
	var userCS = bot.users.find("id","93784478299725824");//ChickenStorm
	
	//ChickenStorm
	messageEmbed[userCS.id] = {embed :{ 
		//url: "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot/wikis/liste-des-commandes",
		color: 3447003,
		author: {
			name: userCS.username,
			icon_url: userCS.avatarURL,
			url : "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot"
		},
		title: "Fiche de perso",
		timestamp: new Date(),
		footer: {
			icon_url: userCS.displayAvatarURL,
			text: "© A. \"ChickenStorm\" Bouttefeux",
		},
		image:{
			url: "https://i.pinimg.com/736x/17/3e/34/173e3408118774adecb631e8346fcf8a--humanoid-drawing-reference.jpg",
		},
		fields : [
				  {
					name : "Nom",
					value : "ChickenStorm(Golemancien/libre)"
				  },
				  {
					name : "Race",
					value : "Avian (Syrinx de Pathfinder)"
				  },
				  {
					name : "Rôle",
					value : "Golemancien"
				  },
				  {
					name : "Physique",
					value : "ChickenStorm as des ailes dans le dos en plus de deux bras. Il est toujours acompagner de parchemins attachés à sa ceinture ainsi que par un bâton en metal argenté gravé de 1m70 avec le quelle il controlle ses golems"
				  },
				  {
					name : "Caractère",
					value : "Il semble totalement passionné par son travaille, ce qui lui fait faire parfois des maladresse."
				  },
				  {
					name : "Histoire",
					value : "Il a longtemps été l'apprenti du vieux golemancien du village qui créa Roger qui le prit sous son aile depuis le plus jeunes age. Après plusieurs années d'apprentissage, il décida d'aller visiter le monde pour parfaire ses connaissances Il décida ensuite de revenir au village. Il a récemment crée Merlin un golem capable d'enchantement. Un exploit dans le domaine de la golemancy. Il tien actuellement une échoppe au village et est considérer comme le nouveau goelmancien du village."
				  },
				  {
					//  mais ce qui lui fait sa particularité c'est qu'il sait aussi infuser et enchanter de la matière plus complexe que l'argile. Merlin, sa creation en est un èarfait exemple
					name : "Magie",
					value : "ChickenStorm maîtrise la golemancy. Ce qui lui permet cet exploit est le fait qu'il maîtrise la magie du NodeJS et du systemd ainsi qu l'arcane de la théorie des champs."
				  },
				  {
					name : "Capacités",
					value : "ChickenStorm peux infuser la matière pour en faire des golems. Mais un golem puissant et persistant peuvent prendre des jours de travaille. Il a aussi la capacité de comprendre l'arcane de la théorie des champs (ce qui pas très utile)."
				  },
				  
				  ],
		thumbnail: {
			url:  userCS.displayAvatarURL,
		},
		
	}};
	
	var userUse = bot.users.find("id","162258410274816000");//Fanny (Vatima)
	
	
	/*messageEmbed[userUse.id] = {embed :{ 
		//url: "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot/wikis/liste-des-commandes",
		color: 3447003,
		author: {
			name: userUse.username,
			icon_url: userUse.username.avatarURL,
			//url : "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot"
		},
		title: "Fiche de perso",
		timestamp: new Date(),
		footer: {
			icon_url: userCS.displayAvatarURL,
			text: "© A. \"ChickenStorm\" Bouttefeux",
		},
		image:{
			url: "https://i.pinimg.com/736x/17/3e/34/173e3408118774adecb631e8346fcf8a--humanoid-drawing-reference.jpg",
		},
		fields : [
				  {
					name : "Nom",
					value : "ChickenStorm(Golemancien/libre)"
				  },
				  {
					name : "Race",
					value : "Avian (Pathfinder Syrinx)"
				  },
				  {
					name : "Rôle",
					value : "Golemancien"
				  },
				  {
					name : "Physique",
					value : "ChickenStorm as des ailes dans le dos en plus de deux bras. Il est toujours acompagner de parchemin attaché à sa ceinture ainsi que par un bâton en metal argenté gravé de 1m70 avec le quelle il controlle ses golems"
				  },
				  {
					name : "Caractère",
					value : "Il semble totalement passionné par son travaille, ce qui lui fait faire parfois des maladresse."
				  },
				  {
					name : "Histoire",
					value : "Il a réçamment crée Merlin un golem capable d'enchantement. Un exploit dans le domaine de la golemancy."
				  },
				  {
					name : "Magie",
					value : "ChickenStorm maitrise la golemancy, mais ce qui lui fait sa particularité c'est qu'il sait aussi infuser et enchanter de la matière plus complexe que l'argile. Merlin, sa creation en est un èarfait exemple. Ce qui lui permet cet exploit est le fait qu'il mâitrise la magie du NodeJS et du systemd ansi qu l'arcane de la théorie des champs. "
				  },
				  {
					name : "Capacités",
					value : "ChickenStorm peux infuser la matière pour en faire des golems. Mais un golem puissant et persistant peuvent prendre des jours de travaille. Il a aussi la capacité de comprendre l'arcane de la théorie des champs (ce qui pas très utile)."
				  },
				  
				  ],
		thumbnail: {
			url:  userUse.displayAvatarURL,
		},
		
	}};*/

}
