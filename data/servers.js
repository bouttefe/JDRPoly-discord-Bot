/*

Copyright 2017 A. "ChickenStorm" Bouttefeux


This file is part of JDRPoly-discord-Bot.

    JDRPoly-discord-Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JDRPoly-discord-Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with JDRPoly-discord-Bot.  If not, see <http://www.gnu.org/licenses/>.

*/


exports.JDRServerID= "358266254462812170";
