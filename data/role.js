/*
Copyright 2017 A. "ChickenStorm" Bouttefeux


This file is part of JDRPoly-discord-Bot.

    JDRPoly-discord-Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JDRPoly-discord-Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with JDRPoly-discord-Bot.  If not, see <http://www.gnu.org/licenses/>.


*/


exports.roleRP = "";
exports.roleRPName = "";


var avaliableRoleToAskP = [
	"RolePlayer",
	"Joueur-Magic",
	"MJ",
	"Non French Speaker"
];

exports.avaliableRoleToAsk = avaliableRoleToAskP;

exports.getRoleToAskStr = function(){
	var returnVal = "```";
	for (var i = 0; i< avaliableRoleToAskP.length-1;++i){
		returnVal = returnVal + " - "+avaliableRoleToAskP[i] +"\n";
	}
	returnVal = returnVal +" - "+ avaliableRoleToAskP[avaliableRoleToAskP.length-1]+"```";
	
	return returnVal;
}
exports.getRoleToAskStrFlat = function(){
	var returnVal = "";
	for (var i = 0; i< avaliableRoleToAskP.length-1;++i){
		returnVal = returnVal +avaliableRoleToAskP[i] +", ";
	}
	returnVal = returnVal +""+ avaliableRoleToAskP[avaliableRoleToAskP.length-1];
	
	return returnVal;
}
