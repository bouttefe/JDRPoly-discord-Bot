/*

Copyright 2017 A. "ChickenStorm" Bouttefeux


This file is part of JDRPoly-discord-Bot.

    JDRPoly-discord-Bot is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    JDRPoly-discord-Bot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with JDRPoly-discord-Bot.  If not, see <http://www.gnu.org/licenses/>.

*/

"use strict";

var DiscordClient = require('discord.js'); // API discord
var path = require("path");
var userList =  require(path.join(__dirname, '/../','data/user.js'));
var serverUtils =  require(path.join(__dirname, '/../','data/servers.js'));
var roleUtils =  require(path.join(__dirname, '/../','data/role.js'));
var rpUtils =  require(path.join(__dirname, '/../','data/rp.js'));
var texts =  require(path.join(__dirname, '/../','data/texts-facts.js'));

//var roleUtils =  require(path.join(__dirname, '/../','data/role.js'));
//var commandUtils =  require(path.join(__dirname, '/../','data/command.js'));



var allBotArrayModules;


var bot = new DiscordClient.Client();

var userListFaction = []; // lioste des utilisateurs


var JDRServer; // guild JDRPoly

var developBuild = true

exports.bot = bot; // set by ref ? 

//var email = ""; // email of bot 
//var password = ""; // pass of the bot

 /*********************************************/
 
 
function success(token){
    /*
	 * When login 
	 */
	rpUtils.init(bot);
    console.log("commadBot login sucessful ");
	
}

function err(error){
	/*
	 * if not loged
	 */
    // handle error
    console.log("Error : " + error +"\n exiting");
    setTimeout(function(){process.exit(0)}, 1000);
    
}


exports.init = function(token,allBotArrayPara){
	/*
	 * Init bot
	 */
    allBotArrayModules = allBotArrayPara;
    bot.login(token).then(success).catch(err);
    
    
}

/**********************************************/

bot.on('disconnect', function() { // quand le bot est déconnecté
	bot.login(token).then(success).catch(err);
});

bot.on('ready', function() { // quand le bot est pret
    
    var serverList = bot.guilds.array();//
    
    for (var i in serverList){
		if (serverList[i].id ==serverUtils.JDRServerID ) {
			JDRServer = serverList[i];
		}
    }
    
    
    
});


bot.on('message', function(message) { // quand le bot est pret
    
    //console.log(message.content)
    
    //todo les disable enable ect
    
    for(var i in command){
		if (command[i].testInput(message)) {
			
			var promise = message.react("✅"); // met un emote quand la comande est reconnu
			promise.then(function(){}).catch((m) => {console.log(m);});
			
			command[i].func(message); // exécute la commande si la condition correcte est verifiée
			//logDebug("message","command " + message);
		}
    }
    
    
});
/******************************************/
// user and role test


var isUserOfRole = function(userID,roleID,serverObj){
    // this is  slow but i am not stick with the user list => may write function more rapid
    /**
     * fiunction given a user id and a roleid says if the user has the role on the server
     *
     *
     */
    var userListFactionTemp = serverObj.members.array()
    
    if (userListFactionTemp != undefined) {
		for(var i in userListFactionTemp){
		   
			if (userID ==userListFactionTemp[i].id ) {
				var rolesOfUser = userListFactionTemp[i].roles.array();
				
				for (var j in rolesOfUser){
					
					if (rolesOfUser[j].id == roleID) {
						return true;
					}
				}
			}
		}
    }
    return false;
}

var isAdminFunc = function(userID){
    /**
     * return true if the user is an admin
     */
    
	//TODO refaires
	var returnBool = false
	for (var i in userList.users){
		if (userList.users[i].userID == userID) {
			returnBool = userList.users[i].isAdmin;
		}
	}
	
	return returnBool;
}
var isModoFunc = function(userID){
    /**
     * return true if the user is an modo or above
     */
	var returnBool = false
	for (var i in userList.users){
		if (userList.users[i].userID == userID) {
			returnBool = userList.users[i].isModo;
		}
	}
	
	return returnBool || isAdminFunc(userID);
}



var notBotFunction = function(userID){
    return !(bot.user.id == userID);
    
}

/**********************************************************************************/
// bot related function

var botSendMessage = function(message,channel,options){
    //send message (you can use .then().catch() ..)
    //options is optional
    if (options!= undefined && options!= null) {
		return channel.send(message,options);
    }
    else {
		return channel.send(message);
    }
}

exports.botSendMessage = botSendMessage;


/**************************************************************************************/
// command
// pas trouvez le moyen d'y aller modulairement sans faire passer plein de fonction

function commandC(testInputp,funcp,inputDescriptionp,descrp,showHelpp) {
	/**
	 * Objet commande
	 * sert à gerer les différentes commadnes
	 */
    this.testInput = testInputp; // fonction de test sur l'entrée
    this.func = funcp; // fonction a executer
    this.inputDescription= inputDescriptionp; // aide : affiche l'input demandé
    this.descr = descrp; // aide : afffiche ce que la commande fait
    this.showHelp= showHelpp; // fonction qui détermine si l'aide
}

var truefunc = function(){ // retourne toujours vrai
    return true
}



var testMessageIfFollowedByMentionToBot = function(message,messageToTest){
    /**
     * message : string
     * messageToTest : string
     * test si un message est de la forme "messageToTest @ChickenBot[ ]*"  
     *
     */
    
    var regexpMessage = new RegExp(messageToTest+" <@!"+bot.user.id+">"+"[ ]*");
    if (regexpMessage.test(message) ) {
		return true
    }
    else{
		return false
    }
    
    
    
}

var testMessageIfFollowedByMentionToBotOrAllone = function(message,messageToTest){
    /**
     * message : string
     * messageToTest : string
     * test si un message est de la forme "messageToTest @ChickenBot[ ]*" ou "messageToTest"
     * 
     */
	
	var regexpMessage = new RegExp(messageToTest);
	var regexpMessage2 = new RegExp(""+messageToTest+"[ ]*");
    
    return message==messageToTest || testMessageIfFollowedByMentionToBot(message,messageToTest) ;
    
    
}


var commandPrefix = "!"; // the prefix of all command



var command = [
    new commandC(
		function(message){
			if(testMessageIfFollowedByMentionToBotOrAllone(message.content,commandPrefix+"ping")){
				return true;
		 	}
			else{
				return false;
			}
		},
		function(message){
			botSendMessage("pong : :ping_pong:  my ping is `"+bot.ping+" ms`",message.channel);
		},
		commandPrefix+"ping", "affiche pong",truefunc
    ),
    new commandC(
		function(message){
			// testMessageIfFollowedByMentionToBotOrAllone(message.content,commandPrefix+"") ||testMessageIfFollowedByMentionToBotOrAllone(message.content,commandPrefix+"helpM"
			var regExpHelp = new RegExp("^"+commandPrefix+"helpMerlin");
			var regExpHelp2 = new RegExp("^"+commandPrefix+"helpMerlin ");
			
			var regExpHelpM = new RegExp("^"+commandPrefix+"helpM");
			var regExpHelpM2 = new RegExp("^"+commandPrefix+"helpM ");
			var m = message.content;
			
			if(regExpHelp.test(m) || regExpHelp2.test(m) || regExpHelpM.test(m)|| regExpHelpM2.test(m)){
				return true
			}
			else{
				return false
			}
		},
		function(message){
			/*var messageTemp = "**Liste des commandes**\nGénérale  ```";
			for (var i in command){
				
				if (command[i].showHelp(message)) {
					messageTemp +=  ""+command[i].inputDescription + " : "+command[i].descr+"\n";
				}
			}
			messageTemp +="```";
			messageTemp += "Pour plus d'information, consultez sur le wiki du bot https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot/wikis/liste-des-commandes"
			*/
			
			var filedsM = [];
			for (var i in command){
				
				if (command[i].showHelp(message)) {
					filedsM.push ({"name" : command[i].inputDescription,
								  "value" : command[i].descr,
								  "inline": false
					});
				}
			}
			//console.log(filedsM);
			
			var messageEmbed = {embed :{
				url: "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot/wikis/liste-des-commandes",
				color: 3447003,
				author: {
					name: bot.user.username,
					icon_url: bot.user.avatarURL,
					url : "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot"
				},
				title: "Page d'Aide",
				timestamp: new Date(),
				footer: {
					icon_url: "https://cdn.discordapp.com/avatars/93784478299725824/49fa5e6b2ebbfdac9736790a49000ef7.png?size=2048",
					text: "© A. \"ChickenStorm\" Bouttefeux",
				},
				fields : filedsM,
				thumbnail: {
					url:  bot.user.avatarURL
				},
				
			}};
			
			
			
			var channel;
			message.author.createDM(); // tente de crée un canal en DM (direct message)
			// TODO mieux gérer la Promise
			
			//channel =  message.author.dmChannel;
			
			
			channel = message.author.dmChannel; // le canal de DM s'il existe
			
			var m = message.content
			var mArray = m.split(" ")
			
			for (var i = 0; i<mArray.length;++i){
				if (mArray[i] == "show" || mArray[i] == "-s") {
					channel = message.channel;
				}
			}
			
			
			if (channel == null ) {
				//channel = message.channel;
				var p = message.author.createDM();
				p.then(function(channel){
					if (messageEmbed != undefined && messageEmbed != null) {
						botSendMessage(messageEmbed,channel);
					}
					else{
						botSendMessage("no help to show",channel);
					}
				})
			}else{
				if (messageEmbed != undefined && messageEmbed != null) {
					botSendMessage(messageEmbed,channel);
				}
				else{
					botSendMessage("no help to show",channel);
				}
			}
		
			
		},
		commandPrefix+"helpMerlin [-s,show]", "affiche la liste des commandes. Show : n'envoi pas de DM",truefunc
    ),
    new commandC(
		function(message){
			if(testMessageIfFollowedByMentionToBotOrAllone(message.content,commandPrefix+"exit") && (isAdminFunc(message.author.id) )){
				// TODO revoir le test pour ajouté des personnes authorisé
				return true
			}
			else{
				return false
			}
			
			
		},
		function(message){
			
			botSendMessage("stopping",message.channel);
			setInterval(function(){allBotArrayModules[0].exit();},1000);
			
		},
		commandPrefix+"exit", "arrête le bot (admin)",function(message){return isAdminFunc(message.author.id);}
    ),
    new commandC(
		function(message){
			
			if(testMessageIfFollowedByMentionToBotOrAllone(message.content,commandPrefix+"about")){
				return true
			}
			else{
				return false
			}
		},
		function(message){
			var messageEmbed = {embed :{
				url: "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot",
				color: 3447003,
				author: {
					name: bot.user.username,
					icon_url: bot.user.avatarURL,
					url : "https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot",
				},
				title: "A propos",
				timestamp: new Date(),
				thumbnail: {
					url:  bot.user.avatarURL
				},
				footer: {
					icon_url: "https://cdn.discordapp.com/avatars/93784478299725824/49fa5e6b2ebbfdac9736790a49000ef7.png?size=2048", // the profile avatar URL of ChickenStorm
					text: "© A. \"ChickenStorm\" Bouttefeux",
				},
				fields : [{
							"name": "Bonjour,",
							"inline": false,
							"value": "je suis <@"+bot.user.id+">("+bot.user.username+")"+"\nj'ai été créé le 17 Octobre 2017 par A. \"ChickenStorm\" Bouttefeux pour le serveur discord JDRPoly "
						  },
						  {
							"name": "Git",
							"inline": false,
							"value": "Mon dépôt git se trouve sous https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot"
						  },
								{
							"name": "aide",
							"inline": false,
							"value": "entrez \"!helpMerlin\" pour voir la liste de mes commandes"
						  },
						  ],
				
			}};
			
			//"Bonjour, je suis JDR-Chicken-Bot.\n\n j'ai été créé le 17 november 2017 par A. \"ChickenStorm\" Bouttefeux pour le serveur JDRPoly sur Discord.\n\n"+
			//	   "Mon dépôt git se trouve sous https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot \n\n entrez \"!help\" pour voir la liste de mes commandes"
			
			botSendMessage(messageEmbed,message.channel);
			//TODO modifier
		},
		commandPrefix+"about", "a propos du bot",truefunc
    ),
    new commandC(
		function(message){
			if(testMessageIfFollowedByMentionToBotOrAllone(message.content,commandPrefix+"commande")){
				return true
			}
			else{
				return false
			}
		},
		function(message){
			
			botSendMessage("Une commande pour les gouverner tous ! ",message.channel);
			
			
		
		},
		commandPrefix+"commande", "Les gouverner tous",truefunc
    ),
	new commandC(
		function(message){
			var regExpDevenir = new RegExp("^"+commandPrefix+"devenir$")
			var regExpDevenir2 = new RegExp("^"+commandPrefix+"devenir ")
			if(regExpDevenir.test(message.content)|| regExpDevenir2.test(message.content)) {
				return true;
			}
			else{
				return false;
			}
		},
		function(message){
			console.log(message.content);
			var mContent = message.content.trim();
			var mArray = mContent.split(" ");
			if (mArray.length >=2) {
				
				mArray.shift(); // remove first element
				var messageSecondPart = mArray.join(" ");
				
				var boolHasGivenRole = false;
				for (var i in roleUtils.avaliableRoleToAsk){
					if (roleUtils.avaliableRoleToAsk[i] == messageSecondPart) {
						var boolHasGivenRole = true;
						var guild = message.guild;
						
						if (guild == null || guild == undefined) {
							botSendMessage("Erreur : faite cette commande sur le serveur JDRPoly",message.channel);
						}
						else{
							var role = message.guild.roles.find("name", messageSecondPart);
							if (role != null && role != undefined) {
								
								
								if (message.member.roles.has(role.id)) {
									//botSendMessage("Je vous ai retiré le role "+role.name,message.channel);
									botSendMessage("Vous avez déjà le rôle "+role.name,message.channel);
									//member.removeRole(role);
								}
								else{
									message.member.addRole(role);
									botSendMessage("Je vous ai ajouté le rôle "+role.name,message.channel);
								}
							}
							else{
								botSendMessage("Erreur :  faite cette commande sur le serveur JDRPoly",message.channel);
							}
						}
					}
				}
				if (! boolHasGivenRole) {
					botSendMessage("Ce rôle n'est pas disponible. Les rôles disponibles sont : "+roleUtils.getRoleToAskStr(),message.channel);
				}
				
				
			}
			else{
				botSendMessage("Il me faut un rôle après la commande. Les rôles disponibles sont : "+roleUtils.getRoleToAskStr(),message.channel);
			}
		},
		commandPrefix+"devenir [role]", "Donne le rôle demandé (disponible :"+roleUtils.getRoleToAskStrFlat()+" )",truefunc
    ),
	new commandC(
		function(message){
			var regExpQuitter = new RegExp("^"+commandPrefix+"quitter")
			var regExpQuitter2 = new RegExp("^"+commandPrefix+"quitter ")
			if(regExpQuitter.test(message.content)|| regExpQuitter2.test(message.content)) {
				return true;
			}
			else{
				return false;
			}
		},
		function(message){
			console.log(message.content);
			var mContent = message.content.trim();
			var mArray = mContent.split(" ");
			if (mArray.length >=2) {
				
				mArray.shift(); // remove first element
				
				var messageSecondPart = mArray.join(" ");
				
				var boolHasGivenRole = false;
				for (var i in roleUtils.avaliableRoleToAsk){
					if (roleUtils.avaliableRoleToAsk[i] == messageSecondPart) {
						var boolHasGivenRole = true;
						var guild = message.guild;
						
						if (guild == null || guild == undefined) {
							botSendMessage("Erreur : faite cette commande sur le serveur JDRPoly",message.channel);
						}
						else{
							var role = guild.roles.find("name", messageSecondPart)
							if (role != null && role != undefined) {
								
								
								if (message.member.roles.has(role.id)) {
									botSendMessage("Je vous ai retiré le role "+role.name,message.channel);
									//botSendMessage("Vous avez déjà le role "+role.name,message.channel);
									message.member.removeRole(role);
								}
								else{
									
									botSendMessage("Vous n'avez pas le rôle "+role.name,message.channel);
								}
							}
							else{
								botSendMessage("Erreur : faite cette commande sur le serveur JDRPoly",message.channel);
							}
						}
					}
				}
				if (! boolHasGivenRole) {
					botSendMessage("Ce rôle n'est pas disponible. es rôles disponibles sont : "+roleUtils.getRoleToAskStr(),message.channel);
				}
				
				
			}
			else{
				botSendMessage("Il me faut un rôle après la commande. Les rôles disponibles sont : "+roleUtils.getRoleToAskStr(),message.channel);
			}
		},
		commandPrefix+"quitter [role]", "Enlève le rôle demandé (disponible :"+roleUtils.getRoleToAskStrFlat()+" )",truefunc
    ),
	new commandC(
		function(message){
			var regExpDe = new RegExp("^"+commandPrefix+"[dD][ée]$")
			var regExpDe2 = new RegExp("^"+commandPrefix+"[dD][ée] ")
			if(regExpDe.test(message.content)||regExpDe2.test(message.content)){
				return true;
			}
			else{
				return false;
			}
		},
		function(message){
			console.log(message.content);
			var mContent = message.content.trim();
			var mArray = mContent.split(" ");
			if (mArray.length >=2) {
				
				var maxNumber = parseInt(mArray[1])
				
				if (! isNaN(maxNumber) &&maxNumber >= 1 ) {
					var rndNumber = Math.floor(Math.random()*maxNumber)+1;
					
					var guildMember = message.member;
					
					var nameToShow = "";
					
					if (guildMember != null && guildMember!= undefined) {
						nameToShow = guildMember.displayName
					}
					else{
						nameToShow = message.author.username
					}
					
					botSendMessage(nameToShow +" lance un dé à " +maxNumber.toString() + " faces. Le dé affiche "+rndNumber.toString()+".",message.channel);
				}
				else{
					botSendMessage("Le dé doit au moins avoir une face.",message.channel);
				}
				
				
				
			}
			else{
				botSendMessage("Indiquez le nombre de de faces qu'a le dé.",message.channel);
			}
			
			message.delete(1000);
		},
		commandPrefix+"dé [n]", "Lance un dé à [n] faces",truefunc
    ),
	new commandC(
		function(message){
			var regExpDe = new RegExp("^"+commandPrefix+"[Ff]iche$")
			var regExpDe2 = new RegExp("^"+commandPrefix+"[Ff]iche ")
			if( ( regExpDe.test(message.content) || regExpDe2.test(message.content) ) && isAdminFunc(message.author.id)){ //TODO chnage that when comande is ready
				return true;
			}
			else{
				return false;
			}
		},
		function(message){
			//console.log(message.content);
			//var mContent = message.content.trim();
			//var mArray = mContent.split(" ");
			if (message.channel.type == "dm") {
				botSendMessage("Cette fonctionalité ne marche pas encore en PM. :sweat_smile: ",message.channel);
			}
			else{
				var noErrorWithMention = true;
				
				var  mentions= message.mentions;
				//console.log("mentions")
				if (mentions!= undefined && mentions!= null) {
					
					
					var membersTemp = mentions.members;
					//console.log("membersTemp")
					if (membersTemp!= undefined && membersTemp!= null) {
						
						
						var member = membersTemp.first();
						
						if (member!= undefined && member!= null) {
							//console.log("member")
							
							var messageToSend = rpUtils.messageEmbedArray[member.id]; 
							if ( messageToSend!= undefined && messageToSend!= null) {
								botSendMessage(messageToSend,message.channel);
							}
							else{
								botSendMessage("Fiche de personage indisponible",message.channel);
							}
							
						}
						else{
							noErrorWithMention = false;
						}
					}
					else{
						noErrorWithMention = false;
					}
				}
				else{
					noErrorWithMention = false;
				}
				if (! noErrorWithMention) {
					botSendMessage("Il faut mentioné au mois quelqu'un.",message.channel);
				}
			}
			
		},
		commandPrefix+"fiche [@nom]", "Affiche la fiche de perso de la personne mentionné.",function(message){return isAdminFunc(message.author.id);} //TODO chnage that when comande is ready
    ),
	new commandC(
		function(message){
			var regExpKaam = new RegExp("^"+commandPrefix+"[Kk]aamelott( )*$");
			var regExpQuote = new RegExp("^"+commandPrefix+"[Qq]uote(s)?( )*$");
			if(regExpKaam.test(message.content)||regExpQuote.test(message.content)){
				return true;
		 	}
			else{
				return false;
			}
		},
		function(message){
			botSendMessage(texts.kaamQuotes[Math.floor(Math.random()*texts.kaamQuotes.length)],message.channel);
		},
		commandPrefix+"kaamelott ou "+commandPrefix+"quote", "Affiche une quote de kaamelott.",truefunc
    ),
];

exports.command = command;



exports.isReadyToStop = false;
exports.stop = function(){
	bot.destroy();
	exports.isReadyToStop = true;
}
