npm = npm
npmInstallFlag= install 
npmEndFlag = --save

nodeJsLib = discord.js 

#-----------------------

wget = wget

cp = cp

unzip = unzip



rm = rm
rmRecursiveFlag = -r
rmSilentFlag = -f

sh = sh

runScript = run.sh

mkdir = mkdir

#--------------------------

make = make

#-----------------------------------------------------

all : update

init: update mkAllDir 

run: 
	$(sh) $(runScript)

update : 
	$(git) pull

installLib:
	$(npm) $(npmInstallFlag) $(nodeJsLib)

mkAllDir:
	$(mkdir) data-fiche

