﻿ChickenBot
=====================


Bot discord pour le serveur JDR-Poly.
Utilisez `!helpMerlin` pour voir la liste des commandes ou consultez le [wiki](https://gitlab.gnugen.ch/bouttefe/JDRPoly-discord-Bot/wikis/liste-des-commandes).

How To Contribute
--------------------

Le modèle suit du [git-flow](http://nvie.com/posts/a-successful-git-branching-model/).  
Les issues sur le git avec le label TODO qui ne sont assigné à personnes sont des projets en attente.
Vous pouvez biensur en faire votre projet. Mettez un message dans l'issue pour annoncé que vous travaillé sur le projet (ou assigner le à vous même si vous faite partie du projet). Dans le but d'évitez que plusieur personnes ne travaillent sur la même issue independament.

Dans le cas du Node.js utilisé le mode strict (`'use strict'`). Pour les identations utilisez des tabulations et pas des epaces. 



Invitation Link
--------------------
Pour invité le bot sur votre serveur (certain fonctionalité ne sont disponible que pour le serveur JDR-Poly) utilisez [ce lien](https://discordapp.com/oauth2/authorize?&client_id=368397273744408576&scope=bot&permissions=2147483647)


